<!DOCTYPE html>
<?php 
define('CHEIMG', $config->urls->templates.'/styles/images/');
define('CHETMPCSS', $config->urls->templates.'/styles/') ; 
define('CHETMPJS', $config->urls->templates.'/scripts/') ; 
?>
<html lang="fr">
<head>
<meta charset="utf-8">
	<title><?php echo $page->get("headline|title"); ?></title>
	<meta name="description" content="<?php echo $page->summary; ?>" />
	<meta name="generator" content="ProcessWire <?php echo $config->version; ?>" />
	<?php if($page->template=="frise"): ?>	
			<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>scripts/timeline/css/timeline.css" />
	<?php endif;
	if($page->template=="mosaique"): ?>	
		<link rel="stylesheet" href=<?php echo CHETMPJS.'photomosaic/includes/prettyPhoto/prettyPhoto.css' ?> title="" type="text/css" media="screen" charset="utf-8">
		<link rel="stylesheet" href=<?php echo CHETMPJS.'photomosaic/css/photoMosaic.css' ?> title="" type="text/css" media="screen" charset="utf-8">
	<?php endif; ?>
	<link rel="stylesheet" type="text/css" href="<?php echo CHETMPCSS?>irene.css" />
	<!--[if IE]><link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/ie.css" /><![endif]-->	
	<script type="text/javascript" src="<?php echo CHETMPJS?>jquery-1.10.2.min.js"></script>
	<?php 
	if($page->template=="mosaique"): ?>	
		<script type="text/javascript" src=<?php echo CHETMPJS.'photomosaic/includes/prettyPhoto/jquery.prettyPhoto.js' ?>></script>
		<script type="text/javascript" src=<?php echo CHETMPJS.'photomosaic/js/jquery.photoMosaic.js' ?>></script>
		<script type="text/javascript" src=<?php echo CHETMPJS.'mosaique.js' ?>></script>
	<?php endif; ?>
	<script type="text/javascript" src="<?php echo CHETMPJS?>main.js"></script>
	<!--
	This website is powered by ProcessWire CMF/CMS.
	ProcessWire is a free open source content management framework licensed under the GNU GPL.
	ProcessWire is Copyright 2012 by Ryan Cramer / Ryan Cramer Design, LLC.
	Learn more about ProcessWire at: http://processwire.com
	-->
</head>
<body class="<?php if ($page->name == "home"){echo 'frontale';}else{echo 'interieure';}?>">
<div class="enveloppe">
<div class="suzy">
	<div id="bandeauSup">
			<?php if($page->name == "home"): ?>	
				<img src="<?php echo CHEIMG; ?>chapeau_accueil.jpg" alt="" width="960" height="80" />
				<a href="<?php echo $config->urls->admin; ?>"><img id="irene" src="<?php echo CHEIMG; ?>nom.png" alt="" width="688" height="25" /></a>
				<img src="<?php echo CHEIMG; ?>photoprinc.jpg" alt="" width="960" height="263" id="photoprinc" />
				<img src="<?php echo CHEIMG; ?>medaillon.png" alt="" width="150" height="141" id="medaillon" />
			<?php else: ?>
	<a href="<?php echo $config->urls->root; ?>"><img id="irene" src="<?php echo $config->urls->templates; ?>/styles/images/nom_petit.png" alt="" width="288" height="32" /></a>
			<div id="navigationInterieure">
			<div><ul>
				<?php 				
				$homepage = $pages->get("/"); 
				$children = $homepage->children;
				echo '<li><a href="'.$homepage->url.'">Accueil</a></li>';
				$i=1;
				$nombreMax = 3;
				foreach($children as $child) {
					if($i >= $nombreMax){echo '</ul></div><div><ul>';$i=0;}
					$iden = str_replace('/','', $child->url);
					$iden = str_replace('-','', $iden);
					echo "<li><a id={$iden} href='{$child->url}'>{$child->title}</a></li>";
					$i++;
				}
				
				echo '</ul></div></div>';
				

			?></div>
			<div id="ariane">
			<ul><?php

				// Create breadcrumb navigation by cycling through the current $page's
				// parents in order, linking to each:

				foreach($page->parents as $parent) {
					echo "<li><a href='{$parent->url}'>{$parent->title}</a></li>";
				}
				echo '<li>'.$page->title.'</li>';

			?></ul>

			<?php endif; ?>
</div>



			
