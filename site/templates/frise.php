<?php 

/**
 * Page template
 *
 */
include("./elements/entete.inc"); 
?>
<div class="titre"><h1><?php echo $page->title; ?></h1></div>
<div class="contenu">
<div class="presentation"><?php echo $page->body; ?></div>
<div id="timeline">
<?php

$lesDates = array();
$lesEres = array();
$timeline = array();
$jSon = array();
$ere=false;
$timeline['headline'] = $page->title;
$timeline['type'] = 'default';
$timeline['text'] = $page->body;
$asset = array();
$asset['media'] = $page->photo->url;
$asset['caption'] = $page->photo->description;
$timeline['asset'] = $asset;
$evenements = $page->children;
foreach ($evenements as $evenement){
	$ere = false;
	$item = array();
	 $item['startDate'] = $evenement->date_depart;
	 $fin = $evenement->date_fin;
	 if($fin!=""){
	 		$item['endDate'] = $fin;
	 		$ere=true;
	 	}
	 $item['headline'] = $evenement->title;
	 $item['text'] = $evenement->texte_frise;
	 $photo = $evenement->photo; 
	$asset = array();
	 if(!$photo==""){
	 	$asset['media'] = $photo->url;
	 	$asset['thumbnail'] = $photo->size(30,30)->url;  
	 	$asset['caption'] = $photo->description;
  	 }/*
else{
	 	$asset['media'] = $config->urls->templates.'styles/images/timeline.jpg';
	 	$asset['thumbnail'] = $config->urls->templates.'styles/images/timeline_tb.jpg';  
  	 }
*/
 	$item['asset'] = $asset;
 	if($ere){
	 	$lesEres[] = $item;
  		$lesDates[] = $item;
 	}else{
  		$lesDates[] = $item;
  	}
}

$timeline['date'] = $lesDates;
$timeline['era'] = $lesEres;
$jSon['timeline'] = $timeline;
$jSon = json_encode($jSon);
echo '<div id="json">'.$jSon.'</div>';
?>

<div>
<div id="timeline-embed"></div>
<!--  <script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/timeline/js/timeline-min.js"></script> -->
 <script type="text/javascript">
	 var donnees = $.parseJSON($('#json').text());
        var timeline_config = {
           width: '100%',
            height: '600',
            source: donnees,
            embed_id: 'timeline-embed', //OPTIONAL USE A DIFFERENT DIV ID FOR EMBED
            start_at_end: false, //OPTIONAL START AT LATEST DATE
            start_at_slide: '1', //OPTIONAL START AT SPECIFIC SLIDE
            start_zoom_adjust: '2', //OPTIONAL TWEAK THE DEFAULT ZOOM LEVEL
            hash_bookmark: true, //OPTIONAL LOCATION BAR HASHES
            debug: false, //OPTIONAL DEBUG TO CONSOLE
            lang: 'fr', //OPTIONAL LANGUAGE
        }
    </script>
 <script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/timeline/js/storyjs-embed.js"></script>
</div>
<?php 
include("./elements/pied.inc"); 
 ?>

