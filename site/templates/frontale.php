<?php

/**
 * Home template
 *
 */

include("./elements/entete.inc");  ?>
<div class="contenu">
<div id='topnav'><?php
$homepage = $pages->get("/");
$children = $homepage->children;
$i = 0;
foreach($children as $child) {
	$i++;
	echo '<div id="menu0'.$i.'"><a href="'.$child->url.'">'.$child->title.'</a></div>';
}

?></div>
	<?php echo $page->body; ?>
</div>
<?php include("./elements/pied.inc"); ?>
