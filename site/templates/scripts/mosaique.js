jQuery(function($) {
	$(document).ready(function() {
/*
		var sCt = $('.sCt');
		var Ct = $('.ct');
		Ct.each(function(){
			$(this).find('a').click(function(){
				sCt.addClass('inv');
 				$(this).find('.sCt').removeClass('inv').fadeIn('slow'); 
			})
		})
*/
		var source = $('#source').html();
		var srcjSON = $(source).find('photo');
		var uneGallerie = [];
		srcjSON.each(function(inc) {
			uneGallerie.push({
				src: $(this).find('src').text(),
				alt: $(this).find('title').text(),
				width: $(this).find('width').text(),
				height: $(this).find('height').text(),
				caption: $.trim($(this).find('tag').text())
			})
		})
		$('#mosaique').photoMosaic({
			input: 'json',
			gallery: uneGallerie,
			external_links: false,
			columns: 6,
			padding: 1,
			// lightbox settings
			modal_name: 'prettyPhoto',
			modal_ready_callback: function($pm) {
				$('a[rel^="prettyPhoto"]', $pm).prettyPhoto({
					overlay_gallery: false,
					slideshow: 5000,
					theme: "pp_default",
					deeplinking: false,
					social_tools: "",
					show_title: true,
					default_width: 800,
					default_height: 800,
					opacity: 0.8
				});
			}
		}) /* fin photomosaic */
	});
})