<?php 

/**
 * Page template
 *
 */
include("./elements/entete.inc"); 
?>
<div class="contenu">
<div class="titre"><h1><?php echo $page->title; ?></h1></div>
	<div id="colG">
		<div id="navigationLaterale">
			<?php
				$parent = $page->parent; 				
				$niveau = count(explode('/', $page->url))-1;
				if($niveau>=3){
					echo '<div class="retour"><a href="'.$parent->url.'">&lt; RETOUR</a></div>';
				}
				$children = $parent->children;
				foreach($children as $child) {
					if($child==$page && $child->numChildren){
					echo '<div class="ct"><a id="'.$iden.'" href="'.$child->url.'">'.$child->title.'</a><div class="sCt2">';
						foreach($child->children as $enfant){
							$iden = str_replace('/','', $enfant->url);
							$iden = str_replace('-','', $iden);
							echo '<div class="ct2"><a id="'.$iden.'" href="'.$enfant->url.'">'.$enfant->title.'</a></div>';
						}
						echo '</div></div>';
					}else{
					$iden = str_replace('/','', $child->url);
					$iden = str_replace('-','', $iden);
					echo '<div class="ct"><a id="'.$iden.'" href="'.$child->url.'">'.$child->title.'</a></div>';
				}
				}
				?>
		</div>
			<?php echo $page->lateral; ?>
	</div>

<div id="colD">
<div class="texteM"><?php if(!$page->body){echo '&nbsp;';}else{echo $page->body;}?></div>
<div id="mosaique"></div>
</div>
<?php 
if($page->numChildren){
	echo '<div class="enveSec">';
	foreach($page->children as $enfant){
		echo '<div class="sectionPhoto"><h3>'.$enfant->title.'</h3>';
		echo '<a href="'.$enfant->url.'"><img src="'.$enfant->images->eq(0)->size(150,200)->url.'" /></a></div>';
	}	
	echo '</div>';
}elseif(count($page->images)<=2){
	echo '<div class="enveSec">';
	foreach($page->images as $image){
		$largeur = $image->width;
		if($largeur > 680){
				echo '<img src="'.$image->width(680)->url.'" />';
		}else{
			echo '<img src="'.$image->url.'" />';
			}
	}
	echo '</div>';
	
}else{
	$arbre = '<div id="source"><?xml version="1.0" encoding="ISO-8859-1"?><photos>';
	foreach ($page->images as $image){
		$arbre .= '<photo><title>'.$image->description.'</title><src>'.$image->url;
		$arbre .= '</src><width>'.$image->width.'</width>';
		$arbre .= '<height>'.$image->height.'</height>';
		unset($cla);
		$tags = $image->tags;
		if($tags){
			$cla .= $tags.' ';
			}
		else {$cla = '';}
		$arbre .= '<tag>'.$cla.'</tag></photo>';
	}
	$arbre .= '</photos></div>';
	echo $arbre;
}

?>

</div>
<?php include("./elements/pied.inc");  ?>


