<!DOCTYPE html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<title><?php echo $page->get("headline|title"); ?></title>

	<meta name="description" content="<?php echo $page->summary; ?>" />

	<meta name="generator" content="ProcessWire <?php echo $config->version; ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/irene.css" />

	<!--[if IE]>
	<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/ie.css" />
	<![endif]-->	

	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/jquery-1.10.2.min.js"></script>

	<script type="text/javascript" src="<?php echo $config->urls->templates?>scripts/main.js"></script>

	<!--
	This website is powered by ProcessWire CMF/CMS.
	ProcessWire is a free open source content management framework licensed under the GNU GPL.
	ProcessWire is Copyright 2012 by Ryan Cramer / Ryan Cramer Design, LLC.
	Learn more about ProcessWire at: http://processwire.com
	-->

</head>
<body>

		<div id="bandeauSup">

			<a href='<?php echo $config->urls->root; ?>'><p id='logo'>ProcessWire</p></a>
		
			<ul id='topnav'><?php
			
				// Create the top navigation list by listing the children of the homepage. 
				// If the section we are in is the current (identified by $page->rootParent)
				// then note it with <a class='on'> so we can style it differently in our CSS. 
				// In this case we also want the homepage to be part of our top navigation, 
				// so we prepend it to the pages we cycle through:
				
				$homepage = $pages->get("/"); 
				$children = $homepage->children;
				$children->prepend($homepage); 
		
				foreach($children as $child) {
					$class = $child === $page->rootParent ? " class='on'" : '';
					echo "<li><a$class href='{$child->url}'>{$child->title}</a></li>";
				}

			?></ul>
</div>
<div id="ariane">
			<ul id='breadcrumb'><?php

				// Create breadcrumb navigation by cycling through the current $page's
				// parents in order, linking to each:

				foreach($page->parents as $parent) {
					echo "<li><a href='{$parent->url}'>{$parent->title}</a> &gt; </li>";
				}

			?></ul>


			<form id='search_form' action='<?php echo $config->urls->root?>search/' method='get'>
				<input type='text' name='q' id='search_query' value='<?php echo htmlentities($input->whitelist('q'), ENT_QUOTES, 'UTF-8'); ?>' />
				<button type='submit' id='search_submit'>Search</button>
			</form>



		</div>


	<div id="content" class="content">
			<h1 id='title'><?php 

				// The statement below asks for the page's headline or title. 
				// Separating multiple fields with a pipe "|" returns the first
				// one that has a value. So in this case, we print the headline
				// field if it's there, otherwise we print the title. 
				
				echo $page->get("headline|title"); 

			?></h1>

		<div class="container">

			<div id="sidebar">

				<?php

				// Output subnavigation
				// 
				// Below we check to see that we're not on the homepage, and that 
				// there are at least one or more pages in this section.
				// 
				// Note $page->rootParent is always the top level section the page is in, 
				// or to word differently: the first parent page that isn't the homepage.

				if($page->path != '/' && $page->rootParent->numChildren > 0) { 

					// We have determined that we're not on the homepage
					// and that this section has child pages, so make navigation: 

					echo "<ul id='subnav' class='nav'>";

					foreach($page->rootParent->children as $child) {
						$class = $page === $child ? " class='on'" : '';
						echo "<li><a$class href='{$child->url}'>{$child->title}</a></li>";	
					}

					echo "</ul>";
				}

				?>

				<div class='sidebar_item'>

					<?php

					// if the current page has a populated 'sidebar' field, then print it,
					// otherwise print the sidebar from the homepage
					
					if($page->sidebar) echo $page->sidebar; 
						else echo $homepage->sidebar; 
					?>


				</div>

			</div><!--/sidebar-->

			<div id="bodycopy">
			
