<?php 

/**
 * Page template
 *
 */
include("./elements/entete.inc");  ?>
<div class="titre"><h1><?php echo $page->title; ?></h1></div>
<div class="contenu">
<p><?php echo $page->date_depart; ?></p>
<div id="texte"><?php echo $page->texte_frise; ?></div>

<?php 
	$photo = $page->photo; 
	if(isset($photo)):?>
	<img src="<?php echo $page->photo->url; ?>" alt="" />
	<img src="<?php echo $page->photo->size(30,30)->url; ?>" alt="" /> 
 	  
<?php endif; ?>


<p><?php echo $page->photo->description; ?></p>
</div>
<?php include("./elements/pied.inc"); ?>

